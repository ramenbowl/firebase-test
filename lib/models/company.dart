import 'package:firebase_database/firebase_database.dart';

class Company {
  String key;
  final bool assignment;
  final String description;
  final String link;
  final String logo;
  final String partner;
  final String street;
  final String town;
  final String zip;

  Company(this.assignment, this.description, this.link, this.logo, this.partner,
      this.street, this.town, this.zip);

  Company.fromSnapshot(DataSnapshot dataSnapshot)
      : key = dataSnapshot.key,
        assignment = dataSnapshot.value["assignment"],
        description = dataSnapshot.value["description"],
        link = dataSnapshot.value["link"],
        logo = dataSnapshot.value["logo"],
        partner = dataSnapshot.value["partner"],
        street = dataSnapshot.value["street"],
        town = dataSnapshot.value["town"],
        zip = dataSnapshot.value["zip"];
  toJson() {
    return {
      "key": key,
      "assignment": assignment,
      "description": description,
      "link": link,
      "logo": logo,
      "partner": partner,
      "street": street,
      "town": town,
      "zip": zip,
    };
  }
}
