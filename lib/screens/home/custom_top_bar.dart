import 'package:flutter/material.dart';

// ignore: non_constant_identifier_names
Widget Custom_Top_Bar() {
  return PreferredSize(
      preferredSize: Size.fromHeight(40.0),
      child: AppBar(
        backgroundColor: Colors.black,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/images/logo.png',
              fit: BoxFit.cover,
              height: 30.0,
            ),
          ],
        ),
        actions: <Widget>[
          InkWell(
              onTap: () {
                //Here implement method what to do when you press the profile button.
                print('Click Profile Pic');
              },
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30),
                child: Image.asset(
                  'assets/images/profile_pic.png',
                ), //Future implementation, get profile picture of the user from database.
              ))
        ],
      ));
}
