import 'package:flutter/material.dart';
import 'package:flutter_application_1/screens/bottomNavigationBarItem/information_page.dart';
import 'package:flutter_application_1/screens/bottomNavigationBarItem/local_search_appbar_page.dart';
import 'package:flutter_application_1/screens/home/custom_top_bar.dart';

class Home extends StatefulWidget {
  @override
  State<Home> createState() => _Home();
}

class _Home extends State<Home> {
  int _selectedIndex = 0;

  // Change the page according to what you choose
  List<Widget> _children = [
    // Automatically become the home page
    InformationPage(),
    // Page to search company information
    LocalSearchAppBarPage(),
  ];

  //Method on what to do when you press one of the 3 buttons.
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      backgroundColor: Colors.white,
      appBar: Custom_Top_Bar(),
      body: _children[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.info),
            label: 'Information',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Search',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.message),
            label: 'Messages',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.red[900],
        unselectedItemColor: Colors.grey[700],
        onTap: _onItemTapped,
        backgroundColor: Colors.black,
      ),
    ));
  }
}
