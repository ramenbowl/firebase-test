import 'package:url_launcher/url_launcher.dart';

//Method where we can launch the google maps app with coordonates.
class MapUtils {

  MapUtils._();

  static Future<void> openMap(double latitude, double longitude) async {
    String googleUrl = 'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Error in launching the app!';
    }
  }
}