import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_application_1/screens/home/Maps/open_google_maps.dart';
import 'package:flutter_application_1/screens/home/custom_top_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

final urlImage = 'https://i.ytimg.com/vi/nKenQG8kQRc/maxresdefault.jpg';

class Accomodation extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: Custom_Top_Bar(),
        body: ListView(
          children: [
            Image.network(urlImage),

            //Info card.
            Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const ListTile(
                    leading: Icon(Icons.info, size: 45),
                    title: Text('Lorem Ipsum'),
                    subtitle: Text(
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum, libero semper viverra rutrum, lectus nulla ultrices sapien, at dapibus turpis quam in lectus. Phasellus volutpat tristique cursus. Aliquam et porta mauris. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur eros ipsum, luctus et tincidunt a, ultrices id tortor. Ut vel ligula ut lacus sollicitudin posuere. Sed lorem metus, scelerisque quis pretium tempus, luctus ac libero. Nulla at rhoncus magna. Maecenas varius, ipsum in luctus vehicula, sem lacus ultricies neque, convallis bibendum ex metus id nisi.'),
                  ),
                ],
              ),
            ),

            //Pictures slider
            Card(
                child: CarouselSlider(
              options: CarouselOptions(
                aspectRatio: 2.0,
                enlargeCenterPage: true,
                enableInfiniteScroll: false,
                initialPage: 2,
                autoPlay: true,
              ),
              items: imageSliders,
            )),

            //Contact card.
            Card(
              semanticContainer: true,
              shadowColor: Colors.black,
              elevation: 15,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: new InkWell(
                child: new Column(
                  children: <Widget>[
                    new ListTile(
                      onTap: () {
                        //Method to launch the google maps app with directions to the specific location using coordinates (latitude, longitude)
                        MapUtils.openMap(52.260851, 6.157569);
                      },
                      leading: new Icon(
                        Icons.navigation,
                        color: Colors.black,
                        size: 26.0,
                      ),
                      title: new Text(
                        "Open navigation",
                        style: new TextStyle(fontWeight: FontWeight.w400),
                      ),
                    ),
                    new Divider(
                      color: Colors.red[900],
                      indent: 16.0,
                    ),
                    new ListTile(
                      //Method to open mail the contact person.
                      onTap: () {
                        launch("mailto:Dimitrescu@gmail.com");
                      },
                      leading: new Icon(
                        Icons.email,
                        color: Colors.black,
                        size: 26.0,
                      ),
                      title: new Text(
                        "Dimitrescu@gmail.com",
                        style: new TextStyle(fontWeight: FontWeight.w400),
                      ),
                    ),
                    new Divider(
                      color: Colors.red[900],
                      indent: 16.0,
                    ),
                    new ListTile(
                      //Method to open and phone call the number.
                      onTap: () {
                        launch("tel://0000000000000");
                      },

                      leading: new Icon(
                        Icons.phone,
                        color: Colors.black,
                        size: 26.0,
                      ),
                      title: new Text(
                        "+0000000000000",
                        style: new TextStyle(fontWeight: FontWeight.w400),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      );
}

//Link for the pictures to be added here in so they can be in the slider
final List<String> imgList = [
  'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
  'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
  'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
  'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
  'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
  'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
];

//The picture slider
final List<Widget> imageSliders = imgList
    .map((item) => Container(
          child: Container(
            margin: EdgeInsets.all(5.0),
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                child: Stack(
                  children: <Widget>[
                    Image.network(item, fit: BoxFit.cover, width: 1200.0),
                    Positioned(
                      bottom: 0.0,
                      left: 0.0,
                      right: 0.0,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(200, 0, 0, 0),
                              Color.fromARGB(0, 0, 0, 0)
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                          ),
                        ),
                        padding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 20.0),
                        child: Text(
                          'No. ${imgList.indexOf(item)} image',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ))
    .toList();
