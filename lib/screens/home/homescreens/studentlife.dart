import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_application_1/screens/home/custom_top_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

final urlImage =
    'https://www.jmu.edu/student-life/_images/student-life-index.jpg';

class StudentLife extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: Custom_Top_Bar(),
        body: ListView(
          children: [
            Image.network(urlImage),

            //Info card.
            Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const ListTile(
                    title: Text('Places to Visit'),
                    subtitle: Text(
                        '1.Twickel is a protected historic country estate with 81 complex parts near Delden in the hamlet of Deldeneresch, in the municipality of Hof van Twente in the province of Overijssel in The Netherlands.'),
                  ),
                  const ListTile(
                    subtitle: Text(
                        '2.Railbiking. Where once rolled steam- and freight trains you can now railbike yourself over real train tracks. Bike across bridges, locks and crossings and taste the atmosphere of 125 years of railroad history in Twente.'),
                  )
                ],
              ),
            ),

            //Pictures slider
            Card(
                child: CarouselSlider(
              options: CarouselOptions(
                aspectRatio: 2.0,
                enlargeCenterPage: true,
                enableInfiniteScroll: false,
                initialPage: 2,
                autoPlay: true,
              ),
              items: placeSliders,
            )),

            Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const ListTile(
                    // leading: Icon(Icons.info, size: 45),
                    title: Text('HENGELO NIGHTLIFE AND PARTIES'),
                    subtitle: Text(
                        'Life can never be boring if you learn to party hard. Do not t just wait for weekends, celebrate every accomplishment or even your existence. Check out the Hengelo nightlife scene with pool parties, discotheques, DJ Nights, pub crawls, trance festivals and many more. You will be surprised to find many happy hours booze and best DJs playing your favorite EDM at weekend party venues near you in Hengelo.Dance to the beats. Explore the parties in Hengelo.'),
                  ),
                ],
              ),
            ),

            Card(
                child: CarouselSlider(
              options: CarouselOptions(
                aspectRatio: 2.0,
                enlargeCenterPage: true,
                enableInfiniteScroll: false,
                initialPage: 2,
                autoPlay: true,
              ),
              items: imageSliders,
            )),
          ],
        ),
      );
}

//Link for the pictures to be added here in so they can be in the slider
final List<String> clubList = [
  'https://10619-2.s.cdn12.com/rests/original/108_501561926.jpg',
  'https://media.indebuurt.nl/hengelo/2017/09/05184026/dux.jpg',
  'https://lh5.googleusercontent.com/p/AF1QipPIECTSwcNY3eKrvt2LOf5PX42t0acaz88S19p7=w500-h500-k-no',
  'https://s3-eu-west-1.amazonaws.com/assets.uitinenschede.nl/locations/t-b%C3%B6lke/_autoHeightLarge/bolke-1731-1542202978-35ht68j31o_200313_150954.jpg?mtime=20200313160955&focal=none&tmtime=20210202104733',
];

final List<String> placeList = [
  'https://www.holland.com/upload_mm/b/8/1/cid57097_fullimage_leadimage%2520kasteel%2520twickel%2520-%2520normal_jpg_10343.jpg',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRdQ8uhE4XoYAxv5hyNtAY5U5xcVEEFTbYE9Q&usqp=CAU',
];

//The picture slider
final List<Widget> imageSliders = clubList
    .map((item) => Container(
          child: Container(
            margin: EdgeInsets.all(5.0),
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                child: Stack(
                  children: <Widget>[
                    Image.network(item, fit: BoxFit.cover, width: 1200.0),
                    Positioned(
                      bottom: 0.0,
                      left: 0.0,
                      right: 0.0,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(200, 0, 0, 0),
                              Color.fromARGB(0, 0, 0, 0)
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                          ),
                        ),
                        padding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 20.0),
                        child: Text(
                          'Club ${clubList.indexOf(item) + 1} ',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ))
    .toList();

final List<Widget> placeSliders = placeList
    .map((item) => Container(
          child: Container(
            margin: EdgeInsets.all(5.0),
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                child: Stack(
                  children: <Widget>[
                    Image.network(item, fit: BoxFit.cover, width: 1200.0),
                    Positioned(
                      bottom: 0.0,
                      left: 0.0,
                      right: 0.0,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(200, 0, 0, 0),
                              Color.fromARGB(0, 0, 0, 0)
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                          ),
                        ),
                        padding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 20.0),
                        child: Text(
                          'Place${placeList.indexOf(item) + 1} ',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ))
    .toList();
