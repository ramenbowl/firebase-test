import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_application_1/models/company.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/widgets.dart';

class LocalSearchAppBarPage extends StatefulWidget {
  @override
  LocalSearchAppBarPageState createState() => LocalSearchAppBarPageState();
}

List<Company> companies = [];
DatabaseReference databaseReference;

class LocalSearchAppBarPageState extends State<LocalSearchAppBarPage> {
  String query = '';

  @override
  void initState() {
    super.initState();
    final FirebaseDatabase database = FirebaseDatabase.instance;
    databaseReference = database.reference().child("company");
    databaseReference.onChildAdded.listen(_onEntryAdded);
    databaseReference.onChildAdded.listen(_onEntryChanged);
  }

  _onEntryAdded(Event event) {
    setState(() {
      companies.add(Company.fromSnapshot(event.snapshot));
    });
  }

  _onEntryChanged(Event event) {
    var old = companies.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });
    setState(() {
      companies[companies.indexOf(old)] = Company.fromSnapshot(event.snapshot);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("search"),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () async {
              showSearch(context: context, delegate: CompanySearch());
            },
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            child: FirebaseAnimatedList(
              query: databaseReference,
              itemBuilder: (BuildContext context, DataSnapshot snapshot,
                  Animation<double> animation, int index) {
                return new ListTile(
                  leading: CircleAvatar(
                    backgroundColor: Colors.blueAccent,
                    child: Image.network(
                      companies[index].logo,
                      errorBuilder: (BuildContext context, Object exception,
                          StackTrace stackTrace) {
                        return Text('No image');
                      },
                    ),
                  ),
                  title: Text(companies[index].partner),
                  subtitle: Text(companies[index].zip),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class CompanySearch extends SearchDelegate<String> {
  final companies = [
    'McDonnals',
    'KFC',
    'Jolibee',
    'Texas Chicken',
    'Chick Fil A',
  ];

  final recentCompanies = [
    'McDonnals',
    'KFC',
    'Jolibee',
  ];

  @override
  List<Widget> buildActions(BuildContext context) => [
        IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            if (query.isEmpty) {
              close(context, null);
            } else {
              query = '';
              showSuggestions(context);
            }
          },
        )
      ];

  @override
  Widget buildLeading(BuildContext context) => IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () => close(context, null),
      );

  @override
  Widget buildResults(BuildContext context) => Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.location_city, size: 120),
            const SizedBox(height: 48),
            Text(
              query,
              style: TextStyle(
                color: Colors.black,
                fontSize: 64,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      );

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestions = query.isEmpty
        ? recentCompanies
        : companies.where((company) {
            final companyLower = company.toLowerCase();
            final queryLower = query.toLowerCase();

            return companyLower.startsWith(queryLower);
          }).toList();

    return buildSuggestionsSuccess(suggestions);
  }

  Widget buildSuggestionsSuccess(List<String> suggestions) => ListView.builder(
        itemCount: suggestions.length,
        itemBuilder: (context, index) {
          final suggestion = suggestions[index];
          final queryText = suggestion.substring(0, query.length);
          final remainingText = suggestion.substring(query.length);

          return ListTile(
            onTap: () {
              query = suggestion;

              // 1. Show Results
              showResults(context);
              // 3. Navigate to Result Page
              //  Navigator.push(
              //   context,
              //   MaterialPageRoute(
              //     builder: (BuildContext context) => ResultPage(suggestion),
              //   ),
              // );
            },
            leading: Icon(Icons.location_city),
            // title: Text(suggestion),
            title: RichText(
              text: TextSpan(
                text: queryText,
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
                children: [
                  TextSpan(
                    text: remainingText,
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );
}
